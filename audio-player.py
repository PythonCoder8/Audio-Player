#Tkinter/Pygame Audio Player

#Import all required modules
import pygame
import tkinter as tk
import os

#Initialize Pygame module
pygame.init()

#Wait for user to enter a file path
fp = input("Enter a file path for a song: ")

#Not allow invalid file path
while os.path.exists(fp) == False:
    fp = input("File path invalid. Enter a valid file path: ")

#Create Tkinter window
window = tk.Tk()

#Give window a size of 200 x 200 pixels
window.geometry("400x295")

#Add "Audio Player" as a title for the window
window.title("Audio Player")

#Create variable with icon for window
icon = tk.PhotoImage(file = "gui-audio-icon.ico")

#Add icon to window
window.iconphoto(False, icon)

#Create label showing which file it is currently playing
currently_playing = tk.Label(window, text = "Currently playing %s" %(fp))

#Create function to run for Tkinter button
def bactionplay():
    pygame.mixer.music.load(fp)
    pygame.mixer.music.play()
    currently_playing.pack()

#Create button to play song
playbtn = tk.Button(window, text = "Play song", command=bactionplay)

#Create button to pause song
pausebtn = tk.Button(window, text = "Pause song", command=pygame.mixer.music.pause)

#Create button to resume song
resumebtn = tk.Button(window, text = "Resume song", command=pygame.mixer.music.unpause)

#Create button to exit window
exitbtn = tk.Button(window, text = "Exit window", command=window.destroy)

#Create button to stop song so if user plays song, song will start from beginning
stopbtn = tk.Button(window, text = "Stop music", command=pygame.mixer.music.stop)

#Add blank text to leave space between buttons
blanklabel = tk.Label(window, text = "")

#Add second piece of blank text to leave space between buttons
blanklabel2 = tk.Label(window, text = "")

#Add third piece of blank text to leave space between buttons
blanklabel3 = tk.Label(window, text = "")

#Add fourth piece of blank text to leave space between buttons
blanklabel4 = tk.Label(window, text = "")

#Add fifth piece of blank text to leave space between buttons
blanklabel5 = tk.Label(window, text = "")

#Display "playbtn" on window
playbtn.pack()

#Display "blanklabel" on window
blanklabel.pack()

#Display "pausebtn" on window
pausebtn.pack()

#Display "blanklabel2" on window
blanklabel2.pack()

#Display "resumebtn" on window
resumebtn.pack()

#Display "blanklabel3" on window
blanklabel3.pack()

#Display "exitbtn" on window
exitbtn.pack()

#Display "blanklabel4" on window
blanklabel4.pack()

#Display "stopbtn" on window
stopbtn.pack()

#Display "blanklabel5" on window
blanklabel5.pack()

#Use tkinter mainloop function to always keep window open until user exits window
window.mainloop()
