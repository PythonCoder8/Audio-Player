# audio-player.py

audio-player.py is programmed with only python.

Launched on October 10th 2020.

This program lets the user enter a file path for an audio file for the program to play the file. Using this code you can pause and resume music.

There are around 6 songs for testing the code in the folder audio-test-files.

**Modules Used for Python Program:**

**1.** Pygame
**2.** Tkinter
**3.** OS

To run this program without any errors you need python installed of course, but you also need pygame installed to load and play the audio files.
Use the instructions below for Python and Pygame installations.

**Install Python for Windows, Linux/Unix Distros, and Mac OS**

To install python for Windows go to https://www.python.org/downloads/windows/.


To install python for Mac OS X go to https://www.python.org/downloads/mac-osx/.


To install python for Linux/Unix go to https://www.python.org/downloads/source/


**Install Pygame for Windows, Linux/Unix Distros, and Mac OS from the Command Prompt**

Type the following on the command prompt to install Pygame for Windows:


`py -m pip install -U pygame --user`


Type the following in the terminal for a mac os x installation:


`python3 -m pip install -U pygame==2.0.0.dev12 --user`


On Debian/Ubuntu/Mint Linux type the following on the command prompt:


`sudo apt-get install python3-pygame`


On Fedora or Red hat type the following on the command prompt:


`sudo yum install python3-pygame`


On Arch/Manjaro Linux type the following on the command prompt:


`sudo pamac install python-pygame`


Raspberry Pis come with pygame pre-installed.
If you try displaying or running files which are not audio files (such as .txt, .jpeg, or .html) you will get a pygame.error.
Note that playing video files will give you a pygame.error.


**Tkinter and OS**

Tkinter and OS both come pre-installed with Python3.

**Versions**

Pygame version: Any

Python version: 3.8 and up

**CaptureAudioPlay.PNG is only used to load to the Github Pages documentation for this repo**

**CODE DEVELOPED BY: PythonCoder8 (Ryan) from Oakville, Canada**
